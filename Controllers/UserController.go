package Controllers

import (
	"errors"
	"fmt"
	"go-api/project/Models"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
)

//constructor
func getErrorMsg(fe validator.FieldError) string {
	switch fe.Tag() {
	case "required":
		return "This field is required"
	case "email":
		return "Should be email format "
	case "e164":
		return "Should be phone number format ex. +62xxxxx "
	case "min":
		return "Should be min length " + fe.Param()
	}
	return "Unknown error"
}

func GetUsers(c *gin.Context) {
	var user []Models.User

	err := Models.GetAllUsers(&user)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"statusCode": 200, "Message": "data is null"})
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user, "statusCode": 200, "Message": "Success Get Data"})
	}
} //CreateUser ... Create User
func CreateUser(c *gin.Context) {
	var user Models.User
	err := c.BindJSON(&user)
	if err != nil {
		fmt.Println(err.Error())
		// c.JSON(http.StatusInternalServerError, gin.H{"statusCode": 500, "Message": err.Error()})
		var ve validator.ValidationErrors
		if errors.As(err, &ve) {
			out := make([]Models.ErrorMsg, len(ve))
			for i, fe := range ve {
				out[i] = Models.ErrorMsg{fe.Field(), getErrorMsg(fe), 500}
			}
			c.JSON(http.StatusInternalServerError, gin.H{"errors": out})
		}
	} else {
		Models.CreateUser(&user)
		c.JSON(http.StatusOK, gin.H{"data": user, "statusCode": 201, "Message": "data successfully created"})
	}
} //GetUserByID ... Get the user by id
func GetUserByID(c *gin.Context) {
	id := c.Params.ByName("id")
	var user Models.User
	err := Models.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"statusCode": 200, "Message": "data is null"})
	} else {
		c.JSON(http.StatusOK, gin.H{"data": user, "statusCode": 200, "Message": "Success Get Data"})
	}
} //UpdateUser ... Update the user information
func UpdateUser(c *gin.Context) {
	var user Models.User
	id := c.Params.ByName("id")
	err := Models.GetUserByID(&user, id)
	if err != nil {
		c.JSON(http.StatusNotFound, user)
	}
	c.BindJSON(&user)
	err = Models.UpdateUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, user)
	}
} //DeleteUser ... Delete the user
func DeleteUser(c *gin.Context) {
	var user Models.User
	id := c.Params.ByName("id")
	err := Models.DeleteUser(&user, id)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"id" + id: "is deleted"})
	}
}
