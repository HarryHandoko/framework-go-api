package Models

type User struct {
	Id      uint   `JSON:"id"`
	Name    string `JSON:"name" binding:"required"`
	Email   string `JSON:"email" binding:"required,email"`
	Phone   string `JSON:"phone" binding:"required,e164"`
	Address string `JSON:"address" binding:"min=10"`
}

type ErrorMsg struct {
	Field      string `json:"field"`
	Message    string `json:"message"`
	StatusCode int    `json:"statusCode"`
}

func (b *User) TableName() string {
	return "user"
}
