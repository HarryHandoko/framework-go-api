package Routes

import (
	"go-api/project/Controllers"

	"github.com/gin-gonic/gin"
)

func GuidMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header["Token"][0]
		key := "dokoganteng"

		if token != key {
			c.JSON(500, gin.H{"Message": "Token is Invalid"})
			c.Abort()
		} else {
			c.Next()
		}

	}
}

func SetupRouter() *gin.Engine {
	r := gin.Default()
	grp1 := r.Group("/user-api").Use(GuidMiddleware())
	{
		grp1.GET("user", Controllers.GetUsers)
	}

	grp2 := r.Group("/s-api")
	{
		grp2.POST("user", Controllers.CreateUser)
		grp2.GET("user/:id", Controllers.GetUserByID)
		grp2.PUT("user/:id", Controllers.UpdateUser)
		grp2.DELETE("user/:id", Controllers.DeleteUser)
	}
	return r
}
